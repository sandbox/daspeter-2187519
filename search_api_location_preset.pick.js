

(function ($) {

  var geocoder;

  if (typeof(google) != 'undefined') {
    geocoder = new google.maps.Geocoder();
  }

  Drupal.searchApiLocationPreset = {
    maps: [],
    markers: [],
    circles: [],

    /**
     * Set the latitude and longitude values to the input fields
     * And optionally update the address field
     *
     * @param LatLng
     *   a location (LatLng) object from google maps api
     * @param i
     *   the index from the maps array we are working on
     * @param op
     *   the op that was performed
     */
    codeLatLong: function(LatLng, i, op) {

      // Update the lat and long input fields
      $('#'  + i + '-lat').val(LatLng.lat());
      $('#'  + i + '-long').val(LatLng.lng());

      // Update the address field
      if ((op == 'marker' || op == 'geocoder') && geocoder) {

        geocoder.geocode({ 'latLng' : LatLng }, function(results, status) {

          if (status == google.maps.GeocoderStatus.OK) {
            $("#" + i + "-address").val(results[0].formatted_address);
          }
          else {
            $("#" + i + "-address").val('');
            if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
              alert(Drupal.t('Geocoder failed due to: ') + status);
            }
          }
        });
      }
    },

    /**
     * Get the location from the address field
     *
     * @param i
     *   the index from the maps array we are working on
     */
    codeAddress: function(i) {
      var address = $("#" + i + "-address").val();

      geocoder.geocode( { 'address': address }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          var map = Drupal.searchApiLocationPreset.maps[i];
          if (map) {
            Drupal.searchApiLocationPreset.maps[i].setCenter(results[0].geometry.location);
          }
          Drupal.searchApiLocationPreset.setMapMarker(results[0].geometry.location, i);
          Drupal.searchApiLocationPreset.codeLatLong(results[0].geometry.location, i, 'textinput');
        } else {
          alert(Drupal.t('Geocode was not successful for the following reason: ') + status);
        }
      });
    },

    /**
     * Set/Update a marker on a map
     *
     * @param latLong
     *   a location (latLong) object from google maps api
     * @param i
     *   the index from the maps array we are working on
     */
    setMapMarker: function(LatLng, i) {
      // Only continue if there's such a map.
      if (typeof(Drupal.searchApiLocationPreset.maps[i]) == 'undefined') {
        return;
      }
      // remove old marker and circle
      if (Drupal.searchApiLocationPreset.markers[i]) {
        Drupal.searchApiLocationPreset.markers[i].setMap(null);
        Drupal.searchApiLocationPreset.circles[i].setMap(null);
      }

      // add marker
      Drupal.searchApiLocationPreset.markers[i] = new google.maps.Marker({
        map: Drupal.searchApiLocationPreset.maps[i],
        draggable: true,
        animation: google.maps.Animation.DROP,
        position: LatLng
      });

      var radius = Drupal.settings.searchApiLocationPreset[i].radius * Drupal.settings.searchApiLocationPreset[i].radius_unit * 1000;
      if ($("#" + i + "-slider").length) {
        radius = $("#" + i + "-slider").slider( "value" ) * Drupal.settings.searchApiLocationPreset[i].radius_unit * 1000;
      }
      // add circle
      //TODO : allow custom colors
      Drupal.searchApiLocationPreset.circles[i] = new google.maps.Circle({
        map: Drupal.searchApiLocationPreset.maps[i],
        clickable:false,
        strokeColor:'#ffcc00',
        fillColor:'#cc3300',
        radius: radius,
        center: LatLng
      });

      // fit the map to te circle
      Drupal.searchApiLocationPreset.maps[i].fitBounds(Drupal.searchApiLocationPreset.circles[i].getBounds());

      return false; // if called from <a>-Tag
    },

    setCurrentCoordinates: function (i, settings, autosumbit, showAlert) {
      if ('geolocation' in navigator) {
        if (!settings) {
          settings = {};
        }
        // Add throbber.
        var $throbber = $('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div>');
        $('#'  + i + '-preset').after($throbber);
        navigator.geolocation.getCurrentPosition(
          function(position) {
            // Update the lat and long input fields.
            $('#'  + i + '-lat').val(position.coords.latitude);
            $('#'  + i + '-long').val(position.coords.longitude);
            // Allow other scripts to react when the current position was set.
            $('#'  + i + '-preset').trigger('currentPositionSet', [ position ] );
            $throbber.remove();
            if (autosumbit) {
              $('#'  + i + '-preset').parents('form').submit();
            }
          },
          function(error) {
            $throbber.remove();
            Drupal.searchApiLocationPreset.handleGeoError(error, i, showAlert);
          },
          settings
        );
      }
    },

    handleGeoError: function(error, i, showAlert) {
      switch(error.code) {
        case error.PERMISSION_DENIED:
          // user denied permission: reset to default search
          //document.location.href = baseUrl;
          var errormsg = Drupal.t('Permission for accessing position denied');
          break;

        default:
        case error.TIMEOUT:
        case error.POSITION_UNAVAILABLE:
          var errormsg = Drupal.t('Current location is not available');
      }
      if (typeof(showAlert) == 'undefined' || showAlert == true) {
        alert(errormsg);
      }
      // Allow other scripts to react when the current position was set.
      $('#'  + i + '-preset').trigger('currentPositionError', [ error ] );
    }
  };

  Drupal.behaviors.searchApiLocationPreset = {
    attach: function (context, settings) {
      var lat;
      var long;
      var LatLng;
      var singleClick;

      // Work on each map
      $.each(settings.searchApiLocationPreset, function(i, presetSettings) {

        // Create slider if the element is there.
        if ($("#" + i + "-slider", context).length) {
          $("#" + i + "-slider", context).slider({
            value: $("#" + i + "-radius").val(),
            min: parseFloat(presetSettings.radius_min),
            max: parseFloat(presetSettings.radius_max),
            step: parseFloat(presetSettings.radius_step),
            slide: function( event, ui ) {
              $("#"+ i +"-radius").val( ui.value );
              if ($("#"+ i +'-gmap').length) {
                Drupal.searchApiLocationPreset.setMapMarker(Drupal.searchApiLocationPreset.markers[i].getPosition(), i);
              }
            },

            stop: function( event, ui ) {
              $("#"+ i +"-radius").val(ui.value);
            }
          });
          // Initialize value and add change handler.
          $("#" + i + "-radius", context)
            .val($("#" + i + "-slider").slider("value"))
            .bind('change.searchApiLocationPreset', function( event) {
              $("#" + i + "-slider").slider("value", this.value);
              Drupal.searchApiLocationPreset.setMapMarker(Drupal.searchApiLocationPreset.markers[i].getPosition(), i);
            });
        }
        // Add preset for users current position.
        if (typeof(presetSettings.current_position) != 'undefined' && presetSettings.current_position.add_preset) {
          if ('geolocation' in navigator) {
            var selected = (presetSettings.preset == 'current') ? ' selected="selected"': '';
            var autosubmit = presetSettings.current_position.autosubmit;
            var getPositionSettings = {
              enableHighAccuracy: parseInt(presetSettings.current_position.enableHighAccuracy) > 0,
              timeout: parseFloat(presetSettings.current_position.timeout),
              maximumAge: (presetSettings.current_position.maximumAge == 'Infinity') ? Infinity : parseFloat(presetSettings.current_position.maximumAge)
            };
            // If there are options available add option for current location.
            if ($("#" + i + "-preset", context).find('option').length > 0) {
              $("#" + i + "-preset", context).append('<option value="current" ' + selected + '>' + Drupal.t('Your position') + '</option>');
              $("#" + i + "-preset", context).bind('change.searchApiLocationPreset', function () {
                if ($(this).val() == 'current') {
                  Drupal.searchApiLocationPreset.setCurrentCoordinates(i, getPositionSettings, autosubmit);
                }
              });
            }
            else {
              // If current is the only preset add a button to fetch the current
              // location.
              var fetchCurrentLocation = function(i, autosubmit, showAlert) {
                if (!presetSettings.current_position.confirm || confirm(presetSettings.current_position.confirm_msg)) {
                  $('#'  + i + '-preset').val('current');
                  // Set radius if radius slider isn't enabled.
                  if ($('#'  + i + '-radius').attr('type') == 'hidden') {
                    $('#'  + i + '-radius').val(presetSettings.current_position.radius.settings.radius);
                  }
                  Drupal.searchApiLocationPreset.setCurrentCoordinates(i, getPositionSettings, autosubmit, showAlert);
                }
              };
              // Add button to fetch current position. A location reset button
              // makes no sense if autorun is enabled.
              if (!selected || presetSettings.current_position.autorun) {
                var button = $('<input  type="button" id="' + i + '-current" class="edit-button form-button ' + i + '-current-position" value="' + Drupal.t(presetSettings.current_position.button_text) + '" />').bind('click.searchApiLocationPreset', function (e) {
                  e.preventDefault();
                  fetchCurrentLocation(i, getPositionSettings, autosubmit, true);
                  return false;
                });
              }
              else {
                // Reset button.
                var button = $('<input  type="button" id="' + i + '-reset" class="edit-button form-button ' + i + '-reset" value="' + Drupal.t('Reset') + '" />').bind('click.searchApiLocationPreset', function (e) {
                  e.preventDefault();
                  $('#'  + i + '-lat').val('');
                  $('#'  + i + '-long').val('');
                  $('#'  + i + '-preset').val('none').parents('form').submit();
                  return false;
                });
              }
              $("#" + i + "-preset", context).after(button);

              // If not selected yet and autorun is enabled fetch position right
              // now but don't show alert when failing.
              if (!selected && presetSettings.current_position.autorun) {
                fetchCurrentLocation(i, getPositionSettings, autosubmit, false);
              }
            }
          }
        }

        // Initialize geocoder.
        $("#" + i + "-geocode", context).click(function(e) {
          Drupal.searchApiLocationPreset.codeAddress(i);
        });
        $("#" + i + "-address", context).keypress(function(ev){
          // Trigger on enter key.
          if (ev.which == 13) {
            ev.preventDefault();
            Drupal.searchApiLocationPreset.codeAddress(i);
          }
        });
        $("#" + i + "-places-address", context).once('process', function() {
          var options = $.extend({
            types: ['geocode']
          }, Drupal.searchApiLocationPreset.placesOptions || {});

          var autocomplete = new google.maps.places.Autocomplete(this, options);
          google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
            if (typeof place.geometry != 'undefined') {
              Drupal.searchApiLocationPreset.codeLatLong(place.geometry.location, i, 'textinput');
            }
          });
        });

        // Create maps for selecting a location.
        $("#"+ i +'-gmap', context).once('process', function(){
          lat = parseFloat(searchApiLocationPreset.lat);
          long = parseFloat(searchApiLocationPreset.long);

          LatLng = new google.maps.LatLng(lat, long);

          // Create map
          Drupal.searchApiLocationPreset.maps[i] = new google.maps.Map(document.getElementById(i + "-gmap"), {
            zoom: 2,
            center: LatLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false
          });

          if (lat && long) {
            // Set initial marker
            Drupal.searchApiLocationPreset.setMapMarker(LatLng, i);
            Drupal.searchApiLocationPreset.codeLatLong(LatLng, i, 'geocoder');
          }

          // Listener to click
          google.maps.event.addListener(Drupal.searchApiLocationPreset.maps[i], 'click', function(me){
            // Set a timeOut so that it doesn't execute if dbclick is detected
            singleClick = setTimeout(function(){
              Drupal.searchApiLocationPreset.codeLatLong(me.LatLng, i, 'marker');
              Drupal.searchApiLocationPreset.setMapMarker(me.LatLng, i);
            }, 500);
          });

          // Detect double click to avoid setting marker
          google.maps.event.addListener(Drupal.searchApiLocationPreset.maps[i], 'dblclick', function(me){
            clearTimeout(singleClick);
          });

          // Listener to dragged.
          google.maps.event.addListener(Drupal.searchApiLocationPreset.markers[i], 'dragend', function(me){
            Drupal.searchApiLocationPreset.codeLatLong(me.LatLng, i, 'marker');
            Drupal.searchApiLocationPreset.setMapMarker(me.LatLng, i);
          });

        })
      });
    }
  }
}
)(jQuery);

